import java.util.Scanner;
public class Ejer4 {
    public int palabra = 1;

    public Ejer4(){
        Scanner lector = new Scanner(System.in);
        String frase = "";
        System.out.println("Introduce la frase:");
        frase = lector.nextLine();
        contadorPalabra(frase);


    }
    public int contadorPalabra(String frase){
        boolean esPalabra = false;
        int numpalabra = 0;
        for(int i = 0; i < frase.length(); i++){
            char c = frase.charAt(i);
            if(Character.isSpaceChar(c) || c == '\n' || c == '\t'){
                esPalabra = false;


            }else if (!esPalabra && Character.isLetter(c)){
                esPalabra = true;
                palabra++;
            }

        }
        return numpalabra;
    }



    public void contadorVocalesYconsonantes( String frase) {
        int contadorConsonante = 0;
        int contadorvocal = 0;

        frase = frase.toLowerCase();
        frase = frase.replaceAll("\\s+","");
        for (int i = 0; i < frase.length(); i++) {
            if (frase.charAt(i) == 'a') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'e') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'i') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'o') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'u') {
                contadorvocal++;
            }else{
                contadorConsonante++;
            }


        }
        System.out.println("Para palabra " + palabra + " el numero de vocales son: " + contadorvocal + " y el numero de consonantes son: " + contadorConsonante);

    }
}
