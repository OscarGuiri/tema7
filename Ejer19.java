import java.util.Scanner;
import java.util.Random;
public class Ejer19{
public Scanner lector = new Scanner(System.in);
public Ejer19(){
     int[] numeros = new int[100];
     System.out.print("{ ");
     for (int i = 0; i < numeros.length; i++){
        numeros[i] = numeroAleatorio(0,100);
        System.out.print(numeros[i] + " ");

    }
    System.out.println(" }");
    System.out.println("Multiplos de 5: ");
    arrayMultiplos5(numeros);
    
    System.out.println("Multiplos de 10: ");
    arrayMultiplos10(numeros);

    System.out.println("Todos los ceros del array");
    imprimeZero(numeros);
}
    /**
     *  Devuelve un numero aleatorio entre 2 numeros introducidos
     * @param minimo El valor minimo
     * @param maximo El valor maximo
     * @return Un numero aleatorio
     */
    public static int numeroAleatorio(int minimo, int maximo){
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo+minimo-1) + minimo;
        return aleatorio;

    }
    /**
     * Imprime los multiplos de 5 de un array
     * @param numeros
     */
    public static void arrayMultiplos5(int[] numeros){
        System.out.print("{ ");

        for (int i = 0; i < numeros.length; i++){
          if(numeros[i] % 5 == 0){
              System.out.print(numeros[i] + " ");
          }  
    
        }
        System.out.println(" }");


    }
    /**
     * imprime los multiplos de 10 de un array
     * @param numeros
     */
    public static void arrayMultiplos10(int[] numeros){
        System.out.print("{ ");

        for (int i = 0; i < numeros.length; i++){
          if(numeros[i] % 10 == 0){
              System.out.print(numeros[i] + " ");
          }  
    
        }
        System.out.println(" }");

    }
    /**
     * Imprime los ceros de un array
     * @param numeros
     */
    public static void imprimeZero(int[] numeros){
        System.out.print("{ ");

        for (int i = 0; i < numeros.length; i++){
          if(numeros[i] == 0){
              System.out.print(numeros[i] + " ");
          }  
    
        }
        System.out.println(" }");


    }
}