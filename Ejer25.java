import java.util.Scanner;
import java.util.Random;
public class Ejer25 {
    public static Scanner lector = new Scanner(System.in);
    public Ejer25(){
        int[] v = new int[50];
        int contadorPar = 0;
        int contador = 0;
        
        for(int i = 0; i < v.length; i++){ // Le da un numero aleatorio a cada elemento del array.
            v[i] = numeroAleatorio(1, 100);

        }
        for(int i = 0; i < v.length; i++){
            if(v[i] % 2 == 0){
                contadorPar++;
            }

        }
        int[] p = new int[contadorPar];
        for(int i = 0; i < p.length; i++){
            if(v[i] % 2 == 0){
                p[contador] = v[i]; 
                contador++;
            }

        }
        imprimirArray(v);
        if(contadorPar == 0){
            System.out.println("No hay numeros pares en el array v");
        }else{
            imprimirArray(p);
        }

        

    }
    /**
     * Genera un numero aleatorio con de tipo int.
     * @param minimo El valor minimo.
     * @param maximo El valor maximo.
     * @return Un int aleatorio.
     */
    public static int numeroAleatorio(int minimo, int maximo) {
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo + minimo - 1) + minimo;
        return aleatorio;

    }
    /**
     * Imprime un array introducido
     * @param numeros
     */
    public void imprimirArray(int[] numeros) {
        System.out.print("[ ");
        for (int j = 0; j <= numeros.length - 1; j++) {
            System.out.print(numeros[j] + " ");
        }
        System.out.println("]");

    }
}