import java.util.Scanner;
import java.util.Random;
public class Ejer16{
    public Scanner lector = new Scanner(System.in);
    public double[] notas = new double[30];
     


    public Ejer16(){
       
        Random r = new Random();
        double aleatorio = 0;
        
        for(int i= 0; i <= notas.length - 1; i++){
            
            notas[i] = 0 + (10 + 0) * r.nextDouble();
           
        }
        System.out.println("Notas superiores a 5: ");
        for(int j = 0; j <= notas.length - 1; j++){
            
            if(notas[j] >= 5){
               System.out.printf("%.2f" ,notas[j]);
               System.out.println();
            }
        }
        
        
        System.out.println("La nota media es " + calculaMedia(notas));
    }
    
    public double calculaMedia(double[] notas) {
        double notaMedia;
        double aux = 0;
        for(int i = 0; i<= notas.length - 1; i++){
            aux += notas[i];
        }
        notaMedia = aux/ notas.length;
        
        return notaMedia;


    }

}