import java.util.Scanner;
public class Ejer10{
    public Scanner lector = new Scanner(System.in);
    public Ejer10(){
        String frase;
        System.out.println("Introduce la frase");
        frase = lector.nextLine();
        if(comprobaPalindroma(frase)){
            System.out.println("La frase " + frase + " es palindroma");
        }else{
            System.out.println("La frase " + frase + " NO es palindroma");
        }
    }
    /**
     * 
     * @param frase la frase que introduce el usuario 
     * @return true si es palandroma, sino, false
     */
    public boolean comprobaPalindroma(String frase){
        frase = frase.replaceAll(" ", "");
        frase = frase.toLowerCase();
        int i = 0;
        int j = frase.length()-1;
        while(i<j){
            if(frase.charAt(i) != frase.charAt(j)){
                return false;
            }
            i++;
            j--;
        }
		return true;


    }
}