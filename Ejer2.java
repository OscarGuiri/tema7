import java.util.Scanner;
public class Ejer2 {
    public Ejer2(){
        Scanner lector = new Scanner(System.in);
        String frase = "";
        System.out.println("Introduce la frase");
        frase = lector.nextLine();

        numeroVocalesYconsonantes(frase);
    }
    public void numeroVocalesYconsonantes( String frase) {
        int contadorvocal = 0;
        int contadorConsonante = 0;
        frase = frase.toLowerCase();
        frase = frase.replaceAll("\\s+","");
        for (int i = 0; i < frase.length(); i++) {
            if (frase.charAt(i) == 'a') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'e') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'i') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'o') {
                contadorvocal++;
            } else if (frase.charAt(i) == 'u') {
                contadorvocal++;
            }else{
                contadorConsonante++;
            }


         }
        System.out.println("El numero de vocales son: " + contadorvocal + " y el numero de consonantes son: " + contadorConsonante);
    }
}

