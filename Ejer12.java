import java.util.Scanner;
public class Ejer12 {
    public Scanner lector = new Scanner(System.in);
    public Ejer12(){

        String frase = "";
        System.out.println("Introduce la frase");
        frase = lector.nextLine();
        System.out.println(eliminarNumeros(frase));
        System.out.println(cambiarEs(frase));
    }
    public String eliminarNumeros(String frase){
        frase = frase.replaceAll("\\d+", "*");

        return frase;

    }
    public String cambiarEs(String frase){
        frase = frase.replaceAll("es", "no por");
        return frase;

    }
}
