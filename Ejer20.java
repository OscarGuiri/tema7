import java.util.Scanner;
import java.util.Random;


    public class Ejer20{
    public Scanner lector = new Scanner(System.in);
    public Ejer20(){
        int numeroIntroducido = 0;
        int[] array = new int[100];
        System.out.print("{ ");
        for (int i = 0; i < array.length; i++){
            array[i] = numeroAleatorio(0,100);
            System.out.print(array[i] + " ");

         }
         System.out.println("\nIntroduce el numero que quieres probar: ");
         numeroIntroducido = Integer.parseInt(lector.nextLine());
         comprobaElementoArray(array, numeroIntroducido);
     }
     /**
     *  Devuelve un numero aleatorio entre 2 array introducidos
     * @param minimo El valor minimo
     * @param maximo El valor maximo
     * @return Un numero aleatorio
     */
    public static int numeroAleatorio(int minimo, int maximo){
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo+minimo-1) + minimo;
        return aleatorio;

    }
    public static void comprobaElementoArray(int[] array, int numeroIntroducido){
        boolean enArray = false;
        for (int i = 0; i < array.length; i++){
          if(array[i] == numeroIntroducido){
              enArray = true;
              
          }

         }
         if(enArray){
             System.out.println("El numero " + numeroIntroducido + " si está en el array");
             System.out.println("Está en posicion ");
            for (int i = 0; i < array.length; i++){
                if(array[i] == numeroIntroducido){
                 System.out.println(i);
                }
  
            }

         }
         

    }
}