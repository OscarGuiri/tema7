import java.util.Scanner;
import java.util.Random;
public class Ejer18{
    public Scanner lector = new Scanner(System.in);
    public Ejer18(){
        
        

        int[] valores1 = new int[10];
        int[] valores2 = new int[10];
        int[] valoresSumados = new int[10];
        int[] valoresDivididos = new int[10];
        System.out.println("Array numero 1: ");
        System.out.print("{ ");
        for (int i = 0; i < valores1.length; i++){
            valores1[i] = numeroAleatorio(0,10);
            System.out.print(valores1[i] + " ");

        }
        System.out.println(" }");
        System.out.println("Array numero 2:");
        System.out.print("{ ");
        for (int i = 0; i < valores1.length; i++){
            valores2[i] = numeroAleatorio(0,10);
            System.out.print(valores2[i] + " ");
        }
        System.out.println(" }");

        valoresSumados = arraySumado(valores1, valores2); //Utilizo un metodo para sumar los dos arrays
        System.out.println("----------SUMA---------");
        System.out.print("{ ");
        for(int i = 0; i < valoresSumados.length; i++){
            System.out.print(valoresSumados[i] + " ");

        }
        System.out.println("\n--------DIVISION-------");
       
        valoresDivididos = arrayDivision(valores1, valores2); // Utilizo un metodo para dividir los 2 arrays
        System.out.print("{ ");
        for(int i = 0; i < valoresDivididos.length; i++){
            System.out.print(valoresDivididos[i] + " ");

        }
        System.out.println(" }");

    }
    /**
     *  Devuelve un numero aleatorio entre 2 numeros introducidos
     * @param minimo El valor minimo
     * @param maximo El valor maximo
     * @return Un numero aleatorio
     */
    public static int numeroAleatorio(int minimo, int maximo){
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo+minimo-1) + minimo;
        return aleatorio;

    }
    /**
     * Suma 2 arrays, devulviendo un array con la suma del contenido de cada elemento de los arrays
     * @param array1 el primer array
     * @param array2  el segundo array
     * @return devuelve un array  sumado
     */
    public static int[] arraySumado(int[] array1, int[] array2){
        int[] arraySumado = new int[10];
        
        if(array1.length > array2.length){
            for(int i = 0; i< array1.length;i++){
                 arraySumado[i] = array1[i] + array2[i];
            }
        }else{
            for(int i = 0; i < array2.length;i++){
                arraySumado[i] = array1[i] + array2[i];
           }
        }
        return arraySumado;


    }
     /**
      * Divide dos arrays elemento por elemento(ESTE METODO NECESITA TRABAJO)
      * @param array1
      * @param array2
      * @return
      */
    public static int[] arrayDivision(int[] array1, int[] array2){
        int[] arrayDividido = new int[10];
        
        if(array1.length > array2.length){
            for(int i = 0; i< array1.length;i++){
                if(array1[i] >= 0 && array2[i] >= 0){
                    arrayDividido[i] = array1[i] / array2[i];
                }
                
            }
        }else{
            for(int i = 0; i < array2.length;i++){
                if(array1[i] > 0 && array2[i] > 0){
                    arrayDividido[i] = array1[i] / array2[i];
                }
           }
        }
        return arrayDividido;


    }



}