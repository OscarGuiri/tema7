import java.util.Scanner;
public class Ejer3 {

    public Ejer3(){
        Scanner lector = new Scanner(System.in);
        String frase = "";
        System.out.println("***Contador de palanbras***");
        System.out.println("Introduce la frase:");
        frase = lector.nextLine();
        System.out.println("El numero de palabras es: " + contadorPalanras(frase));

    }
    public int contadorPalanras(String frase){
        int contador = 1;
        for(int i = 0; i < frase.length(); i++){
            if(frase.charAt(i) == ' '){
                contador++;
            }
        }
        return contador;
    }
}
