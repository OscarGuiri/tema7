import java.util.Scanner;
import java.util.Random;
public class Ejer24 {
    public static Scanner lector = new Scanner(System.in);
    public Ejer24() {
        int[] v = new int[50];
        int[] p = new int[50];
        int aux = 0;
        for (int i = 0; i < v.length; i++) {
            v[i] = numeroAleatorio(1, 9);
        }
        for (int i = 0; i < v.length; i++) {
            aux += v[i];
            p[i] = aux;
        }
        imprimirArray(v);
        imprimirArray(p);

    }
    public static int numeroAleatorio(int minimo, int maximo) {
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo + minimo - 1) + minimo;
        return aleatorio;

    }
    public void imprimirArray(int[] numeros) {
        System.out.print("[ ");
        for (int j = 0; j <= numeros.length - 1; j++) {
            System.out.print(numeros[j] + " ");
        }
        System.out.println("]");

    }
}