import java.util.Scanner;
public class Ejer11{
    public Scanner lector = new Scanner(System.in);
    public Ejer11(){
        String nombre;
        String apellido;
        String apellido2;
        String nombreCompleto;
        int longitud;
        
        //Pido los datos
        System.out.println("Introduce tu nombre");
        nombre = lector.nextLine();
        System.out.println("Introduce el apellido");
        apellido = lector.nextLine();
        System.out.println("Introduce tu segundo apellido");
        apellido2 = lector.nextLine();

        nombreCompleto = nombre + " " + apellido + " " + apellido2;
        System.out.println(nombreCompleto);
        nombreCompleto = nombreCompleto.toLowerCase();
        System.out.println(nombreCompleto);
        nombreCompleto = nombreCompleto.toUpperCase();
        System.out.println(nombreCompleto);
        longitud = nombreCompleto.length() - 3;
        System.out.println( "La longitud es " + longitud);
        if(longitud >= 5){
            System.out.println(nombreCompleto.substring(0, 5));
        }
        if(longitud >= 2){
            System.out.println(nombreCompleto.substring(nombreCompleto.length() - 1, nombreCompleto.length() - 2));
        }
    }

}