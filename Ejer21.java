import java.util.Scanner;
import java.util.Random;


    public class Ejer21{
    public static Scanner lector = new Scanner(System.in);

    public int[] array = new int[10];

    public Ejer21() {
        int[] vector = new int[10];
        int input = -1;
        System.out.println("***VISUALIZADOR DE ARRAYS***");

        do {
           input = imprimirMenuPrincipal();
           switch(input){
              case 1:
                crearArray(vector);
                break; 
              case 2:
                visualitzarArray(vector);
                break;
              case 3:
                visualitzarParell(vector);
                break;
              case 4:
                visualitzarMultiples3(vector);
                break;
              default:
                break;
           }
           
        } while (input != 0);
    }

    public static int imprimirMenuPrincipal() {
        int input = -1;
        do{
            System.out.println("MENÚ PRINCIPAL");
            System.out.println("==============");
            System.out.println("1.-Rellenar array.");
            System.out.println("2.-Visualizar contenido del array");
            System.out.println("3.-Visualizar contenido par.");
            System.out.println("4.-Visualizar contenido múltiplo de 3");
            System.out.println("0.-Salir del menú.");
            System.out.println("Selecciona una opción: ");
            
            input = Integer.parseInt(lector.nextLine());
            if(input > 5 ){
                System.out.println("Por favor introduce una opcion valida");
            }
           
        }while(input > 5 || input < 0);
        return input;
    } 
    public static void crearArray(int[] vector){
        for (int i = 0; i < vector.length; i++){
            vector[i] = numeroAleatorio(1, 50);
            }
            System.out.println("Array creado");
    }
     /**
      * genera un numero aleatorio de tipo int
      * @param minimo El numero minimo
      * @param maximo El numero maximo
      * @return
      */
    public static int numeroAleatorio(int minimo, int maximo){
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo+minimo-1) + minimo;
        return aleatorio;

    }
    /**
     * Visualiza el contenido del array.
     * @param vector El array introducido.
     */
    public static void visualitzarArray(int[] vector){
        int contador = 0;
        for (int i = 0; i < vector.length; i++){
            System.out.print("Vecotor numero " + contador);
            System.out.print(" [");
            System.out.print(vector[contador]);
            System.out.println("] ");
            contador++;
            }
            
    }
    /**
     * Visualiza los numeros pares del array/vector.
     * @param vector El array introducido.
     */
    public static void visualitzarParell(int[] vector){
        int contador = 0;
        System.out.println("Los numero pares:");
        for (int i = 0; i < vector.length; i++){
                if(vector[i] % 2 == 0){
                    System.out.print(" Posicion: " + contador);
                    System.out.print(" [");
                    System.out.print(vector[i]);
                    System.out.println("] ");
                }
                contador++;
            }   
    }
    /**
     * Visualiza los multiplos de 3 en el array.
     * @param vector El array introducido.
     */
    public static void visualitzarMultiples3(int[] vector){
        int contador = 0;
        System.out.println("Los numero multiplos de 3:");
        for (int i = 0; i < vector.length; i++){
                if(vector[i] % 3 == 0){
                    System.out.print(" Posicion: " + contador);
                    System.out.print(" [");
                    System.out.print(vector[i]);
                    System.out.println("] ");
                }
                contador++;
            }   
    }
}