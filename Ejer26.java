import java.util.Scanner;
import java.util.Random;
public class Ejer26 {

    public static Scanner lector = new Scanner(System.in);
    public Ejer26(){
        int[][] matriz = new int[4][8];
        for(int i = 0; i < matriz.length;i++){
        
            for(int j = 0; j < matriz[i].length; j++){
                matriz[i][j] = numeroAleatorio(1, 9);
            }
        }
        for(int i = 0; i < matriz.length;i++){
           
            for(int j = 0; j < matriz[i].length; j++){
               System.out.println( matriz[i][j]);
            }
        }

    }
    /**
     * Genera un numero aleatorio con de tipo int.
     * @param minimo El valor minimo.
     * @param maximo El valor maximo.
     * @return Un int aleatorio.
     */
    public static int numeroAleatorio(int minimo, int maximo) {
        Random r = new Random();
        int aleatorio = 0;
        aleatorio = r.nextInt(maximo + minimo - 1) + minimo;
        return aleatorio;

    }

}   